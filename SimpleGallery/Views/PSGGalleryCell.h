//
//  PSGGalleryCell.h
//  SimpleGallery
//
//  Created by Vadym Markov on 4/9/14.
//  Copyright (c) 2014 Pingbull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSGGalleryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
