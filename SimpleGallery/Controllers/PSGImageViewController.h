//
//  PSGImageViewController.h
//  SimpleGallery
//
//  Created by Vadym Markov on 4/9/14.
//  Copyright (c) 2014 Pingbull. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSGImage.h"

@interface PSGImageViewController : UIViewController

@property (nonatomic, strong) PSGImage *image;

@end
