//
//  PSGImageViewController.m
//  SimpleGallery
//
//  Created by Vadym Markov on 4/9/14.
//  Copyright (c) 2014 Pingbull. All rights reserved.
//

#import "PSGImageViewController.h"

@interface PSGImageViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation PSGImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.image) {
        self.imageView.image = [UIImage imageNamed:self.image.name];
        self.title = self.image.title;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
