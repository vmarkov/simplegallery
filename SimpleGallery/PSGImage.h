//
//  PSGImage.h
//  SimpleGallery
//
//  Created by Vadym Markov on 4/9/14.
//  Copyright (c) 2014 Pingbull. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSGImage : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *title;

@end
