//
//  PSGViewController.m
//  SimpleGallery
//
//  Created by Vadym Markov on 4/9/14.
//  Copyright (c) 2014 Pingbull. All rights reserved.
//

#import "PSGViewController.h"
#import "PSGImageViewController.h"
#import "PSGGalleryCell.h"
#import "PSGImage.h"

static NSString *const kPSGViewControllerCollectionCellID = @"GalleryCell";
static NSString *const kPSGViewControllerImageSegue = @"ImageSegue";

@interface PSGViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *images;

@end

@implementation PSGViewController {
    PSGImage * selectedImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = @"Home";
    self.titleLabel.text = [self.titleLabel.text stringByAppendingString:@" v1"];
    
    self.images = [NSMutableArray array];
    for (NSInteger i = 1; i < 8; i++) {
        PSGImage *image = [[PSGImage alloc] init];
        image.name = [NSString stringWithFormat:@"%ld.jpg", (long)i];
        image.title = [NSString stringWithFormat:@"Image %ld", (long)i];
        [self.images addObject:image];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kPSGViewControllerImageSegue] && selectedImage) {
        
        PSGImageViewController *detailViewController = (PSGImageViewController*) segue.destinationViewController;
        detailViewController.image = selectedImage;
        //[self.collectionView deselectItemAtIndexPath:<#(NSIndexPath *)#> animated:<#(BOOL)#>];
    }
}

#pragma mark - UICollection Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.images count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PSGGalleryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPSGViewControllerCollectionCellID forIndexPath:indexPath];
    
    PSGImage *image = (PSGImage *) [self.images objectAtIndex:[indexPath row]];
    
    cell.imageView.image = [UIImage imageNamed:image.name];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PSGImage *image = (PSGImage *) [self.images objectAtIndex:[indexPath row]];
    selectedImage = image;
    [self performSegueWithIdentifier:kPSGViewControllerImageSegue sender:self];
}

@end
